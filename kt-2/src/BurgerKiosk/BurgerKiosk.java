package BurgerKiosk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class BurgerKiosk {
    private Scanner reader;
    ArrayList<Burger> burgerList = new ArrayList<Burger>();

    public BurgerKiosk(Scanner reader) {
        this.reader = reader;

        burgerList = new ArrayList<Burger>();
    }

    private void finishOrder() {
        Double total = 0.00;

        if (!this.burgerList.isEmpty()) {
            for (int index = 0; index < this.burgerList.size(); index++) {
                this.burgerList.get(index).koostaBurger();
                total += this.burgerList.get(index).getBurgerPrice();
            }
        }
    }

    public void start() {
        OrderProcess order = new OrderProcess(reader);
        this.burgerList = order.finishOrder();
        this.finishOrder();
    }

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        BurgerKiosk kiosk = new BurgerKiosk(reader);

        kiosk.start();
    }
}
