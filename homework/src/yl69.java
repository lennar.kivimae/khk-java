import java.util.Scanner;

public class yl69 {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        System.out.println("Type a text: ");
        String text = reader.nextLine();
        if (palindrome(text)) {
            System.out.println("The text is a palindrome!");
        } else {
            System.out.println("The text is not a palindrome!");
        }
    }

    public static String reverse(String text) {
        String reversedString = new StringBuilder(text).reverse().toString();

        return reversedString;
    }

    public static boolean palindrome(String text) {
        return text.equals(reverse(text));
    }
}
