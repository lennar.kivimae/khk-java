public class yl46 {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5, 6};
        System.out.println(average(numbers));
    }
    
    public static double average(int[] numbers) {
        int numbersLength = numbers.length;
        double total = 0;

        for (int number: numbers) {
            total += number;
        }

        return total / numbersLength;
    }
}
