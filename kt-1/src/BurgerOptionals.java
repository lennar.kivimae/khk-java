public class BurgerOptionals {
    private String optionalName;
    private Double price;

    public BurgerOptionals(String optionalName, Double price) {
        this.optionalName = optionalName;
        this.price = price;
    }

    public Double getOptionalPrice() {
        return this.price;
    }

    public String getOptionalName() {
        return this.optionalName;
    }
}
