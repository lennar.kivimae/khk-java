public class Product {
    private Integer price;
    private Integer amount;
    private String name;

    public Product(String name, Integer amount, Integer price) {
        this.name = name;
        this.amount = amount;
        this.price = price;
    }

    public String printProduct() {
        return name + ", amount: " + amount + ", price: " + price;
    }

    public static void main(String[] args) {
        Product banana = new Product("banana", 1, 20);

        System.out.println(banana.printProduct());
    }
}
