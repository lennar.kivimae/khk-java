public class yl1 {
    public static void main(String[] args) {
        int number = 9;

        for (int index = 0; index <= 9; index++) {
            String formattedNumbersAsLine = "";

            for (int i = 9; i >= 0; i--) {
                if (index == 0) {
                    formattedNumbersAsLine += " " + i;

                    continue;
                }

                if (number <= i) {
                    formattedNumbersAsLine += " " + number;

                    continue;
                }

                formattedNumbersAsLine += " " + i;
            }

            number -= 1;
            System.out.println(formattedNumbersAsLine);
        }
    }
}
