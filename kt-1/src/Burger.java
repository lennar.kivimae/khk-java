import java.lang.reflect.Array;
import java.util.ArrayList;

public class Burger {
    private String name;
    private String meat;
    private String bread;
    private Double price;
    private ArrayList<BurgerOptionals> optionals;

    public Burger (String name, String meat, Double price) {
        this.name = name;
        this.meat = meat;
        this.bread = "teraleib";
        this.price = price;
        this.optionals = new ArrayList<BurgerOptionals>();
    }

    public void setBread(String bread) {
        this.bread = bread;
    }

    public void addOptional (String optionalName, Double optionalPrice) {
        BurgerOptionals optional = new BurgerOptionals(optionalName, optionalPrice);
        this.price += optionalPrice;
        this.optionals.add(optional);
    }

    public Double getBurgerPrice() {
        return this.price;
    }

    public void koostaBurger() {
        String currentBurgerString = "Name: " + this.name + ", meat: " + this.meat + ", bread: " + this.bread;
        Double currentBurgerPrice = this.price;

        if (!this.optionals.isEmpty()) {
            currentBurgerString += ", Lisandid:";

            for (int i = 0; i < this.optionals.size(); i++) {
                currentBurgerPrice += this.optionals.get(i).getOptionalPrice();
                currentBurgerString += " " + this.optionals.get(i).getOptionalName();
            }
        }

        currentBurgerString += ", Price: " + currentBurgerPrice;

        System.out.println(currentBurgerString);
    }
}
