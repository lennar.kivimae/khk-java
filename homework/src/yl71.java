import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class yl71 {
    public static void main(String[] args) {
        ArrayList<Integer> list3 = new ArrayList<Integer>();
        ArrayList<Integer> list4 = new ArrayList<Integer>();

        list3.add(4);
        list3.add(3);
        list3.add(5);

        list4.add(5);
        list4.add(10);
        list4.add(4);

        System.out.println(smartCombine(list3, list4));
    }

    public static List smartCombine(ArrayList<Integer> first, ArrayList<Integer> second) {
        Set<Integer> set = new LinkedHashSet<>(first);
        set.addAll(second);
        List<Integer> filteredList = new ArrayList<>(set);

        return filteredList;
    }
}
