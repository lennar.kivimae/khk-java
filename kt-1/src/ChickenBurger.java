import java.util.ArrayList;
import java.util.Scanner;

public class ChickenBurger extends Burger {
    private ArrayList<BurgerOptionals> availableOptionals;

    public ChickenBurger(String name, String meat, Double price) {
        super(name, meat, price);

        this.availableOptionals = new ArrayList<BurgerOptionals>();
        BurgerOptionals cheese = new BurgerOptionals("Juust", 0.50);
        this.availableOptionals.add(cheese);
        BurgerOptionals salad = new BurgerOptionals("Salat", 0.50);
        this.availableOptionals.add(salad);
        BurgerOptionals tomato = new BurgerOptionals("Tomat", 0.50);
        this.availableOptionals.add(tomato);
        BurgerOptionals majo = new BurgerOptionals("Majonees", 0.50);
        this.availableOptionals.add(majo);
    }

    public ArrayList<BurgerOptionals> getAllOptionals() {
        return this.availableOptionals;
    }
}
