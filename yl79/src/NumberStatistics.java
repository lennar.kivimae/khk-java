import java.util.Scanner;

public class NumberStatistics {
    private int amountOfNumbers;
    private int sum;

    public NumberStatistics() {
        this.amountOfNumbers = 0;
    }

    public void addNumber(int number) {
        this.sum += number;
        this.amountOfNumbers += 1;
    }

    public int amountOfNumbers() {
        return this.amountOfNumbers;
    }

    public int sum() {
       return this.sum;
    }

    public double average() {
        return this.sum / this.amountOfNumbers;
    }

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        NumberStatistics all = new NumberStatistics();
        NumberStatistics even = new NumberStatistics();
        NumberStatistics odd = new NumberStatistics();

        for (int index = 0; index < 5; index++) {
            System.out.println("Enter a number: ");
            int input = reader.nextInt();

            if (input > 0) {
                all.addNumber(input);

                if ((input & 1) == 0) {
                    even.addNumber(input);
                } else {
                    odd.addNumber(input);
                }
            }
        }

        System.out.println("Sum: " + all.sum());
        System.out.println("Sum of even " + even.sum());
        System.out.println("Sum of odd " + odd.sum());
    }
}
