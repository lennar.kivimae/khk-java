import java.util.ArrayList;

public class OldFashionedCheeseburger extends Burger {
    private ArrayList<BurgerOptionals> availableOptionals;

    public OldFashionedCheeseburger(String name, String meat, Double price) {
        super(name, meat, price);

        this.availableOptionals = new ArrayList<BurgerOptionals>();

        BurgerOptionals bacon = new BurgerOptionals("Peekon", 0.50);
        this.availableOptionals.add(bacon);

        BurgerOptionals cucumber = new BurgerOptionals("Kurk", 0.50);
        this.availableOptionals.add(cucumber);

        BurgerOptionals onion = new BurgerOptionals("Sibul", 0.50);
        this.availableOptionals.add(onion);

        BurgerOptionals sauce = new BurgerOptionals("Peekoni kaste", 0.50);
        this.availableOptionals.add(sauce);
    }

    public ArrayList<BurgerOptionals> getAllOptionals() {
        return this.availableOptionals;
    }
}
