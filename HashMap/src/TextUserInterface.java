import java.util.Scanner;

public class TextUserInterface {
    private Dictionary dictionary;
    private Scanner reader;

    public TextUserInterface(Scanner reader, Dictionary dictionary) {
        this.dictionary = dictionary;
        this.reader = reader;
    }

    public void init() {
        System.out.println("Enter command:");
        String command = reader.nextLine();
        this.commandHandler(command);
    }

    public void commandHandler(String command) {
        if (command.equals("quit")) {
            System.out.println("Cheers!");

            return;
        }

        if (command.equals("add")) {
            this.add();

            return;
        }

        if (command.equals("translate")) {
            this.translate();

            return;
        }

        System.out.println("Unknown statement");
    }

    public void add() {
        System.out.println("In Finnish:");
        String nativeWord = this.reader.nextLine();

        System.out.println("Translation:");
        String translatedWord = this.reader.nextLine();

        dictionary.add(nativeWord, translatedWord);
        System.out.println("Word: " + nativeWord + ", Translation: " + translatedWord + " - Added!");
    }

    public void translate() {
        System.out.println("Give a word");
        String nativeWord = this.reader.nextLine();

        System.out.println("Translation: " + dictionary.translate(nativeWord));
    }

    public void start() {
        this.init();
    };

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        Dictionary dict = new Dictionary();

        TextUserInterface ui = new TextUserInterface(reader, dict);
        ui.start();
    }
}
