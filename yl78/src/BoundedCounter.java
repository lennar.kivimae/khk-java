import java.util.Scanner;

public class BoundedCounter {
    private int value;
    private int upperLimit;

    public BoundedCounter(int upperLimit) {
        this.value = 0;
        this.upperLimit = upperLimit;
    }

    public void next() {
        if (this.value == this.upperLimit) {
            this.value = 0;

            return;
        }

        this.value += 1;
    }

    public String toString() {
        if (this.value < 10) {
            return "0" + this.value;
        }

        return String.valueOf(this.value);
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static void main(String[] args) throws Exception {
        Scanner reader = new Scanner(System.in);
        BoundedCounter seconds = new BoundedCounter(59);
        BoundedCounter minutes = new BoundedCounter(59);
        BoundedCounter hours = new BoundedCounter(23);

        int[] values = new int[3];

        for(int i = 0; i < values.length; i++) {
            System.out.println("Enter next value: ");
            values[i] = reader.nextInt();
        }

        seconds.setValue(values[0]);
        minutes.setValue(values[1]);
        hours.setValue(values[2]);

        while ( true ) {
            System.out.println( hours + ":" + minutes + ":" + seconds);   // the current time printed
            seconds.next();
            if (seconds.getValue() == 0) {
                minutes.next();

                if (minutes.getValue() == 0) {
                    hours.next();
                }
            }
            Thread.sleep(1000);
        }
    }
}
