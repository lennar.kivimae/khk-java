package BurgerKiosk;

import java.util.ArrayList;

abstract class Burger {
    private String name;
    private String meat;
    private String bread;
    private Double price;
    private ArrayList<BurgerOptionals> optionals;
    private ArrayList<BurgerOptionals> availableOptionals;

    public Burger () {
        this.optionals = new ArrayList<BurgerOptionals>();
        this.availableOptionals = new ArrayList<BurgerOptionals>();
    }

    public void addAvailableOptional(String name, Double price) {
        BurgerOptionals availableOptional = new BurgerOptionals(name, price);

        this.availableOptionals.add(availableOptional);
    }

    abstract void setBurgerOptionals();

    abstract ArrayList<BurgerOptionals> getAvailableOptionals();

    public Burger name (String name) {
        this.name = name;

        return this;
    }

    public Burger meat (String meat) {
        this.meat = meat;

        return this;
    }

    public Burger bread (String bread) {
        this.bread = bread;

        return this;
    }

    public Burger price (Double price) {
        this.price = price;

        return this;
    }

    public void addOptional (String optionalName, Double optionalPrice) {
        BurgerOptionals optional = new BurgerOptionals(optionalName, optionalPrice);
        this.price += optionalPrice;
        this.optionals.add(optional);
    }

    public Double getBurgerPrice() {
        return this.price;
    }

    public void koostaBurger() {
        String currentBurgerString = "Name: " + this.name + ", meat: " + this.meat + ", bread: " + this.bread;
        Double currentBurgerPrice = this.price;

        if (!this.optionals.isEmpty()) {
            currentBurgerString += ", Lisandid:";

            for (int i = 0; i < this.optionals.size(); i++) {
                currentBurgerPrice += this.optionals.get(i).getOptionalPrice();
                currentBurgerString += " " + this.optionals.get(i).getOptionalName();
            }
        }

        currentBurgerString += ", Price: " + currentBurgerPrice;

        System.out.println(currentBurgerString);
    }
}
