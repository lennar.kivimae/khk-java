package BurgerKiosk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class OptionalOrderProcess {
    private Scanner reader;
    private Boolean isFinished;
    private ArrayList<BurgerOptionals> availableOptionals;
    private HashMap<String, Double> chosenOptionals;

    public OptionalOrderProcess(Scanner reader, ArrayList<BurgerOptionals> availableOptionals) {
        this.reader = reader;
        this.isFinished = false;
        this.availableOptionals = availableOptionals;
        this.chosenOptionals = new HashMap<String, Double>();

        this.init();
    }

    private void init() {
        while (!this.isFinished) {
            Scanner optionals = new Scanner(System.in);
            System.out.println("Choose your additions:");

            if (!this.availableOptionals.isEmpty()) {
                System.out.println("[0] Finish adding additions");
                for (int index = 0; index < this.availableOptionals.size() ; index++) {
                    String availableOptionalName = this.availableOptionals.get(index).getOptionalName();
                    Double availableOptionalPrice = this.availableOptionals.get(index).getOptionalPrice();


                    System.out.println("["+ (index + 1) + "] Nimi: " + availableOptionalName + ", Hind: " + availableOptionalPrice);
                }
            }

            int optionalChoice = optionals.nextInt();

            if (optionalChoice == 0) {
                this.isFinished = true;
            } else {
                String optionalName = this.availableOptionals.get(optionalChoice-1).getOptionalName();
                Double optionalPrice = this.availableOptionals.get(optionalChoice-1).getOptionalPrice();

                chosenOptionals.put(optionalName, optionalPrice);
            }
        }
    }

    public HashMap<String, Double> getChosenOptionals() {
        return this.chosenOptionals;
    }
}
