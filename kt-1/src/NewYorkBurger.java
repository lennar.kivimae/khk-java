import java.util.ArrayList;

public class NewYorkBurger extends Burger {
    private ArrayList<BurgerOptionals> availableOptionals;

    public NewYorkBurger(String name, String meat, Double price) {
        super(name, meat, price);

        this.availableOptionals = new ArrayList<BurgerOptionals>();

        BurgerOptionals pineapple = new BurgerOptionals("Ananass", 0.50);
        this.availableOptionals.add(pineapple);

        BurgerOptionals cucumber = new BurgerOptionals("Kurk", 0.50);
        this.availableOptionals.add(cucumber);

        BurgerOptionals onion = new BurgerOptionals("Sibul", 0.50);
        this.availableOptionals.add(onion);

        BurgerOptionals tomato = new BurgerOptionals("Tomat", 0.50);
        this.availableOptionals.add(tomato);
    }

    public ArrayList<BurgerOptionals> getAllOptionals() {
        return this.availableOptionals;
    }
}
