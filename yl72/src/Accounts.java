public class Accounts {
    String account;
    Integer balance;

    public Accounts(String account, Integer balance) {
        this.account = account;
        this.balance = balance;
    }

    public void withdraw(Integer amount) {
        this.balance -= amount;
    }

    public void deposit(Integer amount) {
        this.balance += amount;
    }

    public Integer balance() {
        return this.balance;
    }

    public static void main(String[] args) {
        Accounts bartosAccount = new Accounts("Barto's account", 100);
        Accounts bartosSwissAccount = new Accounts("Barto's account in Switzerland", 1000000);

        System.out.println("Initial state");
        System.out.println(bartosAccount);
        System.out.println(bartosSwissAccount);

        bartosAccount.withdraw(20);
        System.out.println("Barto's account balance is now: "+bartosAccount.balance());
        bartosSwissAccount.deposit(200);
        System.out.println("Barto's Swiss account balance is now: "+bartosSwissAccount.balance());

        System.out.println("Final state");
        System.out.println(bartosAccount);
        System.out.println(bartosSwissAccount);
    }
}
