import java.util.ArrayList;
import java.util.Scanner;

public class BurgerKiosk {
    public static void main(String[] args) {
        Boolean finished = false;
        ArrayList<Burger> burgerList = new ArrayList<Burger>();

        while (!finished) {
            Scanner input = new Scanner(System.in);
            System.out.println("Choose your burger:");
            System.out.println("[0] Finish Order");
            System.out.println("[1] Chicken burger - kanalihaga, valge saiaga.  Lisandite valik koosneb juustust, salatist, tomatist, maioneesist.");
            System.out.println("[2] Old Fashioned Cheeseburger - loomalihaga, valge saiaga.  Lisandite valik koosneb peekonist, kurgist, sibulast ja pekooni kastest.");
            System.out.println("[3] New York Burger - sealihaga, teraleivaga. Lisandite valik koosneb ananassist, tomatist, kurgist ja sibulast.");

            int choice = input.nextInt();

            if (choice == 0 ) {
                finished = true;
                Double total = 0.00;
                if (!burgerList.isEmpty()) {
                    for (int index = 0; index < burgerList.size(); index++) {
                        burgerList.get(index).koostaBurger();
                        total += burgerList.get(index).getBurgerPrice();
                    }

                    System.out.println("Total: " + total);
                }
            }

            if (choice == 1) {
                boolean finishOptionals = false;
                ChickenBurger burger = new ChickenBurger("Chicken burger", "kanaliha", 5.00);
                burger.setBread("Valge sai");

                while (!finishOptionals) {
                    ArrayList<BurgerOptionals> allOptionals = burger.getAllOptionals();
                    Scanner optionals = new Scanner(System.in);
                    System.out.println("Choose your additions:");

                    if (!allOptionals.isEmpty()) {
                        System.out.println("[0] Finish adding additions");
                        for (int index = 0; index < allOptionals.size() ; index++) {
                            System.out.println("["+ ( index + 1) + "] Nimi: " + allOptionals.get(index).getOptionalName() + ", Hind: " + allOptionals.get(index).getOptionalPrice());
                        }
                    }

                    int optionalChoice = optionals.nextInt();

                    if (optionalChoice == 0) {
                        finishOptionals = true;
                    } else {
                        String optionalName = allOptionals.get(optionalChoice-1).getOptionalName();
                        Double optionalPrice = allOptionals.get(optionalChoice-1).getOptionalPrice();
                        burger.addOptional(optionalName, optionalPrice);
                    }
                }

                burgerList.add(burger);

                continue;
            }

            if (choice == 2) {
                boolean finishOptionals = false;
                OldFashionedCheeseburger burger = new OldFashionedCheeseburger("Old fashioned cheese burger", "loomaliha", 12.00);
                burger.setBread("Valge sai");

                while (!finishOptionals) {
                    ArrayList<BurgerOptionals> allOptionals = burger.getAllOptionals();
                    Scanner optionals = new Scanner(System.in);
                    System.out.println("Choose your additions:");

                    if (!allOptionals.isEmpty()) {
                        System.out.println("[0] Finish adding additions");
                        for (int index = 0; index < allOptionals.size() ; index++) {
                            System.out.println("["+ ( index + 1) + "] Nimi: " + allOptionals.get(index).getOptionalName() + ", Hind: " + allOptionals.get(index).getOptionalPrice());
                        }
                    }

                    int optionalChoice = optionals.nextInt();

                    if (optionalChoice == 0) {
                        finishOptionals = true;
                    } else {
                        String optionalName = allOptionals.get(optionalChoice-1).getOptionalName();
                        Double optionalPrice = allOptionals.get(optionalChoice-1).getOptionalPrice();
                        burger.addOptional(optionalName, optionalPrice);
                    }
                }

                burgerList.add(burger);

                continue;
            }

            if (choice == 3) {
                boolean finishOptionals = false;
                NewYorkBurger burger = new NewYorkBurger("Old fashioned cheese burger", "loomaliha", 8.00);

                while (!finishOptionals) {
                    ArrayList<BurgerOptionals> allOptionals = burger.getAllOptionals();
                    Scanner optionals = new Scanner(System.in);
                    System.out.println("Choose your additions:");

                    if (!allOptionals.isEmpty()) {
                        System.out.println("[0] Finish adding additions");
                        for (int index = 0; index < allOptionals.size() ; index++) {
                            System.out.println("["+ ( index + 1) + "] Nimi: " + allOptionals.get(index).getOptionalName() + ", Hind: " + allOptionals.get(index).getOptionalPrice());
                        }
                    }

                    int optionalChoice = optionals.nextInt();

                    if (optionalChoice == 0) {
                        finishOptionals = true;
                    } else {
                        String optionalName = allOptionals.get(optionalChoice-1).getOptionalName();
                        Double optionalPrice = allOptionals.get(optionalChoice-1).getOptionalPrice();
                        burger.addOptional(optionalName, optionalPrice);
                    }
                }

                burgerList.add(burger);

                continue;
            }
        }
    }
}
