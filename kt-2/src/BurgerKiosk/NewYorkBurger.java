package BurgerKiosk;

import java.util.ArrayList;

public class NewYorkBurger extends Burger {
    private ArrayList<BurgerOptionals> availableOptionals;

    public NewYorkBurger() {
        this.availableOptionals = new ArrayList<BurgerOptionals>();
        this.setBurgerOptionals();
    }

    @Override
    public void setBurgerOptionals() {
        BurgerOptionals cheese = new BurgerOptionals("Juust", 0.50);
        this.availableOptionals.add(cheese);
        BurgerOptionals salad = new BurgerOptionals("Salat", 0.50);
        this.availableOptionals.add(salad);
        BurgerOptionals tomato = new BurgerOptionals("Tomat", 0.50);
        this.availableOptionals.add(tomato);
        BurgerOptionals majo = new BurgerOptionals("Majonees", 0.50);
        this.availableOptionals.add(majo);
    }

    @Override
    public ArrayList<BurgerOptionals> getAvailableOptionals() {
        return this.availableOptionals;
    }
}
