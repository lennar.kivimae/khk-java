package BurgerKiosk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class OrderProcess {
    private Scanner reader;
    private Boolean isFinished;
    ArrayList<Burger> burgerList = new ArrayList<Burger>();

    public OrderProcess(Scanner reader) {
        this.isFinished = false;
        this.reader = reader;

        this.init();
    }

    public void init() {
        while (!this.isFinished) {
            Burger burger = null;

            Scanner input = new Scanner(System.in);
            System.out.println("Choose your burger:");
            System.out.println("[0] Finish order");
            System.out.println("[1] Chicken burger - kanalihaga, valge saiaga.  Lisandite valik koosneb juustust, salatist, tomatist, majoneesist.");
            System.out.println("[2] Old Fashioned Cheeseburger - loomalihaga, valge saiaga.  Lisandite valik koosneb peekonist, kurgist, sibulast ja pekooni kastest.");
            System.out.println("[3] New York burger - sealihaga, teraleivaga. Lisandite valik koosneb ananassist, tomatist, kurgist ja sibulast.");

            int choice = input.nextInt();

            if (choice == 0) {
                this.isFinished = true;

                this.finishOrder();
            }

            if (choice == 1) {
                burger = new ChickenBurger().name("Chicken Burger").meat("kanaliha").bread("valge sai").price(5.00);
            }

            if (choice == 2) {
                burger = new OldFashionedCheeseburger().name("Old Fashioned Cheeseburger").meat("loomaliha").bread("valge sai").price(12.00);
            }

            if (choice == 3) {
                burger = new NewYorkBurger().name("New York Burger").meat("loomaliha").bread("teraleib").price(8.00);
            }

            if (burger != null) {
                OptionalOrderProcess optionals = new OptionalOrderProcess(this.reader, burger.getAvailableOptionals());
                HashMap<String, Double> chosenOptionals = optionals.getChosenOptionals();

                for (String optional : chosenOptionals.keySet()) {
                    burger.addOptional(optional, chosenOptionals.get(optional));
                }

                burgerList.add(burger);

                continue;
            }
        }
    }

    public ArrayList<Burger> finishOrder() {
        return this.burgerList;
    }
}
